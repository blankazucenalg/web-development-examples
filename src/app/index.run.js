(function() {
  'use strict';

  angular
    .module('testangulargulp')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();

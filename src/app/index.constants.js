/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('testangulargulp')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
